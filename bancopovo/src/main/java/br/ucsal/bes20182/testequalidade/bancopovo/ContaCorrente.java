package br.ucsal.bes20182.testequalidade.bancopovo;

public class ContaCorrente {

	private Double saldo;

	public ContaCorrente() {
		saldo = 0d;
	}

	public void depositar(Double valor) {
		saldo += valor;
	}

	public void sacar(Double valor) throws SaldoInsuficienteException {
		if (valor > saldo) {
			throw new SaldoInsuficienteException();
		}
		saldo -= valor;
	}

	public Double consultarSaldo() {
		return saldo;
	}

}
