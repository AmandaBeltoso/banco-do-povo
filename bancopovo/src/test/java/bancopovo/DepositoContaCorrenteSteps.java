package bancopovo;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import br.ucsal.bes20182.testequalidade.bancopovo.ContaCorrente;

public class DepositoContaCorrenteSteps {
	
	private ContaCorrente contaCorrenteNova;

	@Given("abri uma conta corrente")
	public void instanciarContaCorrente() {
		contaCorrenteNova = new ContaCorrente();
	}

	@When("realizo um depósito de $deposito")
	public void informarDepositoValor1(Double valor) {
		contaCorrenteNova.depositar(valor);
	}
	

	@Then("o saldo da minha conta será de $saldo")
	public void verificarSaldoContaCorrente(Double saldo) {
		Assert.assertEquals(saldo, contaCorrenteNova.consultarSaldo());
		
	}

}
