Narrativa:
Como um correntista
desejo realizar um depósito
de modo que possa aumentar meu saldo

Cenário: Realizar dois depósitos numa conta nova
Dado que abri uma conta corrente
Quando realizo um depósito de 300 reais
E realizo um depósito de 200 reais
Então o saldo da minha conta será de 500 reais